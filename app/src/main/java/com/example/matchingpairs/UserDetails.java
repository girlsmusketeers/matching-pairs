package com.example.matchingpairs;

import android.app.Application;

public class UserDetails extends Application {

    private String username;
    private String password;
    private String score;

    public UserDetails() {

    }

    public UserDetails(String username, String password,  String score) {
        this.username = username;
        this.password = password;
        this.score = score;
    }

    public String getScore() {return score;}

    public void setScore(String score) {
        this.score = score;
    }

    public String getPassword() {return password;}

    public void setPassword(String parola) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
