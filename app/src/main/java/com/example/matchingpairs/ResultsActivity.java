package com.example.matchingpairs;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {
    ArrayList<UserDetails> users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        ListView listView=(ListView)findViewById(R.id.listView);

        users = new ArrayList<>();

        getData();

        ListAdapter adapter = new ListAdapter(this, R.layout.adapter_view_layout, users);
        listView.setAdapter(adapter);


    }

    public void getData() {
        SQLiteDatabase _dataDB;
        _dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);

        Cursor cursor = _dataDB.rawQuery("SELECT * FROM User", null);

        if (cursor.moveToFirst()) {
            do {
                String strName = cursor.getString(cursor.getColumnIndex("Name"));
                String strPassword = cursor.getString(cursor.getColumnIndex("Password"));
                String strScore = cursor.getString(cursor.getColumnIndex("Score"));

                UserDetails user = new UserDetails(strName, strPassword, strScore);
                users.add(user);
            } while (cursor.moveToNext());

        }
        cursor.close();

        Collections.sort(users, new Comparator<UserDetails>() {
            @Override
            public int compare(UserDetails o1, UserDetails o2) {
                return o1.getScore().compareTo(o2.getScore());
            }
        });

        if(users.size() > 10) {
            List subList = users.subList(0, 10);
           users = new ArrayList<>(subList.size());
            users.addAll(subList);
        }
    }
}
