package com.example.matchingpairs;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText user;
    private EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeDataBase();

        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
    }

    public String getUserScore() {
        String userScore="";
        try{
            SQLiteDatabase _dataDB;
            _dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);
            SQLiteStatement score = _dataDB.compileStatement("SELECT User.Score FROM User " +
                        "WHERE Name='" + user.getText().toString() + "' AND Password = '"
                        + pass.getText().toString() + "';");

            userScore = score.simpleQueryForString();
            _dataDB.close();
            return userScore;
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return userScore;
    }

    public boolean checkDataBase(View view) {

        if(user.getText().toString().length() != 0 && pass.getText().toString().length() != 0) {
            try {
                SQLiteDatabase _dataDB;

                _dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);
                SQLiteStatement s = _dataDB.compileStatement("SELECT count(*) FROM User " +
                        "WHERE Name='" + user.getText().toString() + "' AND Password = '"
                        + pass.getText().toString() + "';");

                long count = s.simpleQueryForLong();
                _dataDB.close();

                if (count >= 1) {

                    UserDetails userDetails = (UserDetails) getApplicationContext();
                    userDetails.setUsername(user.getText().toString());
                    userDetails.setScore(getUserScore());
                    Intent intentMenu = new Intent(this, MainActivity.class);
                    startActivity(intentMenu);
                } else {
                    Toast.makeText(getApplicationContext(), "User/Password not found." + "\n" +
                            "      Try again or register.", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                return false;
            }
            return true;

        } else {
            Toast.makeText(getApplicationContext(), "Invalid user/password values.", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void initializeDataBase() {
        SQLiteDatabase dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);

        dataDB.execSQL("CREATE TABLE IF NOT EXISTS User(Name Text(20) PRIMARY KEY ," + " Password Text(30)," + " Score Text(10));");
        dataDB.close();
    }
    public void goToRegister(View view) {
        Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(registerIntent);
    }

}

