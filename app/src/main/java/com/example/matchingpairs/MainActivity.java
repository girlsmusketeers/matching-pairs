package com.example.matchingpairs;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private List<ImageView> imageList;
    private ImageView firstImage;
    private int firstImageTag;
    private boolean clickEnabled = true;
    private TextView timer;
    private CountDownTimer countDownTimer;
    private long timeLeft = 61000;
    private int matchingPairs = 0;
    private UserDetails userDetails;
    private String scoreFormatted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userDetails = (UserDetails) getApplicationContext();
        String showUser = "Login as: " + userDetails.getUsername();
        Toast.makeText(getApplicationContext(), showUser, Toast.LENGTH_LONG).show();

        timer = findViewById(R.id.timer);
        startTimer();

        setupItems();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_restart) {
            restartGame();
            return true;
        }

        if (id == R.id.action_results) {
            Intent resultsIntent = new Intent(MainActivity.this, ResultsActivity.class);
            startActivity(resultsIntent);
            return true;
        }

        if (id == R.id.action_exit) {
            timeLeft = 0;
            Intent exitIntent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(exitIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void restartGame() {
        for (ImageView img : imageList) {
            img.setImageResource(R.drawable.ic_back);
            img.setVisibility(View.VISIBLE);
        }
        countDownTimer.cancel();
        timeLeft = 61000;
        startTimer();
        setupItems();
        firstImage = null;
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(timeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                if (matchingPairs<6) {
                    Intent newIntent = new Intent(MainActivity.this, GameOverActivity.class);
                    startActivity(newIntent);
                }
            }
        }.start();

    }

    private void updateCountDownText() {
        int minutes = (int) (timeLeft / 1000) / 60;
        int seconds = (int) (timeLeft / 1000) % 60;

        int secondsScore = 61- seconds;
        scoreFormatted = String.format(Locale.getDefault(), "%02d:%02d", 0, secondsScore);

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        timer.setText(timeLeftFormatted);
    }

    private void setupItems() {

        imageList = new ArrayList<>();

        for(int i = 1; i <= 4; i++)
        {
            for(int j = 1; j <= 3; j++) {
                int id = getResources().getIdentifier("id"+i+j, "id", getPackageName());
                imageList.add((ImageView)findViewById(id));
            }

        }


        List<Integer> idImages = new ArrayList<>();
        for(int i = 1; i <= 6; i++) {
            idImages.add(i);
            idImages.add(i);
        }

        Collections.shuffle(idImages);

        for (int i = 0; i < imageList.size(); i++) {
            imageList.get(i).setTag(idImages.get(i));
            imageList.get(i).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {

        if (clickEnabled && (firstImage == null || firstImage != v)) {
            clickEnabled = false;

            final ImageView selectedImage = (ImageView) v;
            switch ((Integer) v.getTag()) {
                case 1:
                    selectedImage.setImageResource(R.drawable.p1);
                    break;
                case 2:
                    selectedImage.setImageResource(R.drawable.p2);
                    break;
                case 3:
                    selectedImage.setImageResource(R.drawable.p3);
                    break;
                case 4:
                    selectedImage.setImageResource(R.drawable.p4);
                    break;
                case 5:
                    selectedImage.setImageResource(R.drawable.p5);
                    break;
                case 6:
                    selectedImage.setImageResource(R.drawable.p6);
                    break;
            }

            if (firstImage == null) {
                firstImage = selectedImage;
                firstImageTag = (Integer) selectedImage.getTag();
                clickEnabled = true;
            } else if (firstImageTag == (Integer) selectedImage.getTag()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        firstImage.setVisibility(View.INVISIBLE);
                        selectedImage.setVisibility(View.INVISIBLE);
                        firstImage = null;
                        clickEnabled = true;
                        matchingPairs++;

                        if(matchingPairs == 6) {
                            timeLeft = 0;
                            Intent newIntent = new Intent(MainActivity.this, CongratulationsActivity.class);
                            checkScore(scoreFormatted, userDetails.getScore());
                            newIntent.putExtra("EXTRA_SCORE", scoreFormatted);
                            newIntent.putExtra("EXTRA_BEST_SCORE", userDetails.getScore());
                            startActivity(newIntent);
                        }
                    }
                }, 500);
            } else if (firstImageTag != (Integer) selectedImage.getTag()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        firstImage.setImageResource(R.drawable.ic_back);
                        selectedImage.setImageResource(R.drawable.ic_back);
                        firstImage = null;
                        clickEnabled = true;
                    }
                }, 500);
            }
        }
    }

    private void checkScore(String score, String bestScore) {
        if(score.compareTo(bestScore) < 0) {
            userDetails.setScore(score);
            try{
                SQLiteDatabase _dataDB;
                _dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);

                String sql = "UPDATE User " + "SET Score ='" + score + "' WHERE Score ='" + bestScore + "' AND Name ='" + userDetails.getUsername() + "';";
                _dataDB.execSQL(sql);
                _dataDB.close();
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
