package com.example.matchingpairs;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    public EditText user;
    public EditText pass;
    public EditText confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
        confirm = findViewById(R.id.confirm_password);
    }

    public void register(View view) {
        if(!pass.getText().toString().equals(confirm.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Assign that the passwords match.", Toast.LENGTH_LONG).show();
        }
        else if(user.getText().toString().length() != 0 && pass.getText().toString().length() != 0) {
            try {
                SQLiteDatabase _dataDB;

                _dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);
                SQLiteStatement s = _dataDB.compileStatement("SELECT count(*) FROM User " +
                        "WHERE Name='" + user.getText().toString() + "' AND Password = '"
                        + pass.getText().toString() + "';");

                long count = s.simpleQueryForLong();
                _dataDB.close();

                if (count >= 1) {
                    Toast.makeText(getApplicationContext(), "This username already exist. \n    " +
                            "Try another username.", Toast.LENGTH_LONG).show();
                }
                else {
                    try{
                        _dataDB = openOrCreateDatabase("MachingPairsDB", MODE_PRIVATE, null);
                        String sql = "INSERT INTO User " +
                                "VALUES('" + user.getText().toString() + "', '" + pass.getText().toString() + "', '" + "01:00" +
                                "');";
                        SQLiteStatement st = _dataDB.compileStatement(sql);
                        st.executeInsert();
                        _dataDB.close();

                        Toast.makeText(getApplicationContext(), "You have registered with success!", Toast.LENGTH_LONG).show();
                        goToLogin();
                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "Invalid user/password values.", Toast.LENGTH_LONG).show();
        }
    }

    public void goToLogin() {
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }
}
