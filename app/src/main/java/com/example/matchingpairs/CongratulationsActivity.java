package com.example.matchingpairs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CongratulationsActivity extends AppCompatActivity {

    private TextView score;
    private TextView bestScore;
    private Button buttonPlayAgain;
    private Button buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);

        score = findViewById(R.id.textView2);
        bestScore = findViewById(R.id.textView3);

        buttonPlayAgain = findViewById(R.id.buttonPlayAgain);
        buttonExit = findViewById(R.id.buttonExit);

        if (getIntent().getExtras() != null) {
            score.setText("Your score: " + getIntent().getExtras().getString("EXTRA_SCORE"));
            bestScore.setText("Your best score: " + getIntent().getExtras().getString("EXTRA_BEST_SCORE"));
        }

        buttonPlayAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(CongratulationsActivity.this, MainActivity.class);
                startActivity(newIntent);
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(CongratulationsActivity.this, LoginActivity.class);
                startActivity(newIntent);
            }
        });
    }
}
